# APT-GET INSTALL POSTMAN #

Postman is the Only Complete API Development Environment

### What is this repository for? ###

* Quick Install Postman via CLI Linux
* [Read Postman Documentation](https://www.getpostman.com/docs/)

### How do I get set up? ###

* Open terminal / an another cli
* type `bash <(wget -O - https://bitbucket.org/creatorb/postman/raw/3eded6646ef818dadeada653bee4bc2696a32d5a/apt-get-install-postman.sh)`
* Waiting Installation completed, Lets Rock!

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact